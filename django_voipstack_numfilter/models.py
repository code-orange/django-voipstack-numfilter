from datetime import datetime

from django.db import models


class VoipNumFilter(models.Model):
    name = models.CharField(max_length=50)
    last_update = models.DateTimeField()

    class Meta:
        db_table = "voip_num_filter"


class VoipNumFilterData(models.Model):
    id = models.BigAutoField(primary_key=True)
    filter = models.ForeignKey(VoipNumFilter, models.CASCADE, db_column="filter")
    num = models.BigIntegerField()
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "voip_num_filter_data"
        unique_together = (("filter", "num"),)
