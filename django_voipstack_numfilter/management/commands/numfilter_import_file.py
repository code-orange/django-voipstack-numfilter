import csv

from django.core.management.base import BaseCommand

from django_voipstack_numfilter.django_voipstack_numfilter.models import *


class Command(BaseCommand):
    help = "Import Num Filter list"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        with open("data/numfilter.csv", "rt") as csvfile:
            reader = csv.reader(csvfile, delimiter=",", quotechar='"')
            for row in reader:
                try:
                    new_num = VoipNumFilterData(
                        num=row[0],
                        filter=VoipNumFilter.objects.get(id=1),
                    )
                    new_num.save(force_insert=True)
                except:
                    pass

                print(row[0])

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
